package widokApp

import org.widok._
import org.widok.html._
import org.widok.bindings.Bootstrap._
import org.widok.bindings.HTML.Label

case class Issue(name: String, id: Long)

object Main extends PageApplication {

  val jsonResult = Var("")
  val hasResult = jsonResult.map { x => x.nonEmpty }
  val name = Var("")
  val hasName = name.map(_.nonEmpty)
  val prodName = "Skan"

  val issues = Buffer[Ref[Issue]]()
  val hasIssues = issues.nonEmpty

  val navbar = NavigationBar(Container(
    NavigationBar.Header(NavigationBar.Toggle(), NavigationBar.Brand(prodName)),
    NavigationBar.Collapse()))

  val qryPane = Grid.Column(
    Label("Enter URL").forId("url"),
    Input.Text().placeholder("URL").id("url").bind(name).size(Size.Large),
    br(),
    Button("Create").onClick(x => jsonResult := FactExtractor.extactFact(name.get)).style(Style.Primary),
    " ",
    Button("Clear").onClick(x => {
      jsonResult := ""
      name := ""
      issues.+=(Ref(FactExtractor.createIssue(name.get)))
    }).style(Style.Primary)).column(Size.Medium, 3)

  lazy val issuesList = Grid.Column(ul(issues.map {
    x => li(x.get.id + " -> " + x.get.name )
  })).show(hasIssues)

  val resultsPane = Grid.Column(h5("Extracted Data in JSON"), jsonResult, issuesList).column(Size.Medium, 9).show(hasResult)
  val container = Container(Grid.Row(qryPane, resultsPane))

  def view() = {
    Inline(navbar, container)
  }

  def ready() {
  }
}

object FactExtractor {

  def extactFact(url: String) = s"""{
  "source" : "$url",
  "date" : "${System.currentTimeMillis()}"},
  "fact-data" : "Lorem Ipsum"
  }"""

  def createIssue(url: String) = Issue(url, System.currentTimeMillis())
}